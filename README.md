# M5Stack-FAQ

*FAQ documents content of M5Stack*

1. [M5Stack MasterController FAQ](https://github.com/watson8544/M5Stack-FAQ/blob/master/M5Stack_MasterControllerBoard_FAQ.md)

2. [M5Stack Module FAQ](https://github.com/watson8544/M5Stack-FAQ/blob/master/M5Stack_Module_FAQ.md)