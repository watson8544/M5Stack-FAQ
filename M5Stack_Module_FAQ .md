# M5Stack Module FAQ

## ESP32CAM

### 1. " can not flash ESP32CAM "

*Specific decription:*

Here is the output from trying to program the module:
```
Flashing binaries to serial port /dev/ttyUSB0 (app at offset 0x10000)...
esptool.py v2.3.1
Connecting........_____....._____....._____....._____....._____....._____....._____....._____....._____....._____
A fatal error occurred: Failed to connect to ESP32: Timed out waiting for packet header
~/esp/esp-idf/components/esptool_py/Makefile.projbuild:54: recipe for target 'flash' failed
make: *** [flash] Error 2
The terminal process terminated with exit code: 2

```

*Solution:*

*It means the ESP32CAM board can not connect with PC serial port*

Step 1:
we suggest that you might resolder the Type-C interface. Maybe it is a bad welding at the interface.

*if the problem has not been solved, please follow step 2*

Step 2:
Add a 2.2uF capacitance as shown below.

![image](FAQ%20Pictures/ESP32CAM_can_not_connect_PC_serial_port.png)






